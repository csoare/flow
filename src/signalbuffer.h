#ifndef SIGNALBUFFER_H
#define SIGNALBUFFER_H

#include <generalbuffer.h>

#include <utility>
#include <string>

class SignalBuffer : public GeneralBuffer<std::pair<double, double>>
{
public:
	enum Domain { Time, Frequency, Single };

    SignalBuffer(int count, const std::string& name, Domain domain)
        : GeneralBuffer<std::pair<double, double>>(count)
        , name(name)
        , domain(domain) {}

    SignalBuffer(const SignalBuffer& sb)
        : GeneralBuffer<std::pair<double, double>>(sb)
        , name(sb.name)
        , domain(sb.domain) {}

    const std::string name;
    const Domain domain;

private:

};

#endif // SIGNALBUFFER_H


#include "datamanager.h"

#include "pluginmanager.h"
#include "mainwindow.h"

#include <QDebug>
#include <thread>

using namespace std;

class DataWatchdog : public ThreadTask
{
public:
    DataWatchdog(unsigned int interval);
    ~DataWatchdog();

    void start();
    void reset();
    void setInterval(unsigned int interval);
    void stop();

private:
    void run();

    unsigned int interval;
    bool valid;
};

DataWatchdog::DataWatchdog(unsigned int interval)
    : interval(interval)
{

}

DataWatchdog::~DataWatchdog()
{
    stop();
    if (thr.joinable())
        thr.join();
}

void DataWatchdog::start()
{
    valid = true;
    ThreadTask::start();
}

void DataWatchdog::reset()
{
    valid = true;
}

void DataWatchdog::setInterval(unsigned int interval)
{
    this->interval = interval;
}

void DataWatchdog::run()
{
    while (ongoing) {
        valid = false;
        this_thread::sleep_for(chrono::microseconds(interval));

        if (!valid) {
            MainWindow::getInstance()->notifyStop();
            ongoing = false;
        }
    }
}

void DataWatchdog::stop()
{
    if (!ongoing)
        return;

    reset();
    ongoing = false;
}

//////////////////////////// DataEntry /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DataEntry::DataEntry(const std::string& name, Domain domain)
	: SignalBuffer(100000, name, domain)
    , nodes(make_shared<vector<DataNode*>>()) {}

DataEntry::DataEntry(const DataEntry& dentry)
    : SignalBuffer(dentry)
    , nodes(dentry.nodes) {}

DataEntry::~DataEntry()
{

}

void DataEntry::add(double x, double y)
{
    while (!GeneralBuffer::add(make_pair(x, y)))
        beginWrite();
}

void DataEntry::endWrite()
{
    GeneralBuffer::endWrite();
    for (DataNode* node : *nodes)
        node->notify();
}

void DataEntry::addDataNode(DataNode* dn)
{
    nodes->push_back(dn);
}

//////////////////////////////////////////////////////////////////////////////////
/////////////////////// DataNode /////////////////////////////////////////////////

DataNode::DataNode(shared_ptr<IPluginGraph> plugin)
    : plugin(plugin)
    , written(false)
{
    for (const auto& i : plugin->getIOProp().second) {
        outputs.push_back(std::make_unique<DataEntry>(i.first, i.second));
        (*plugin)[i.first] = outputs.back().get();
    }
}

DataNode::~DataNode()
{

}

void DataNode::completeInputs(DataEntry& dentry)
{
    for (const auto& i : plugin->getIOProp().first) {
        qDebug() << "completing" << QString::fromStdString(dentry.name);
        inputs.push_back(std::make_unique<DataEntry>(dentry));
        inputs.back()->addDataNode(this);
        (*plugin)[i.first] = inputs.back().get();
    }
}

DataEntry& DataNode::getOut(bool side, int idx)
{
    return side ? *inputs[idx] : *outputs[idx];
}

void DataNode::notify()
{
    lock_guard<mutex> lock(mut);
    cvar.notify_one();
    written = true;
}

void DataNode::start()
{
    plugin->reset();
    for (auto& i : outputs)
        i->setWriterEnabled(true);

    plugin->start();
    ThreadTask::start();
}

void DataNode::stop()
{
    ongoing = false;
    notify();
    for (auto& i :  outputs)
        i->setWriterEnabled(false);

    ThreadTask::stop();
    plugin->stop();
}

int DataNode::getWritten()
{
    int sum = 0;
    for (auto& i : inputs)
        sum += i->beginRead(true, false);
    return sum;
}

void DataNode::run()
{
    while (ongoing) {
        unique_lock<mutex> lock(mut);
        while (!inputs.empty() && !written)
            cvar.wait(lock);
        written = false;
        lock.unlock();

        getWritten();
        for (auto& i : outputs)
            i->beginWrite();

        plugin->run();

        for (auto& i : inputs)
            i->endRead();
        for (auto& i : outputs)
            i->endWrite();
    }
}

//////////////////////////////////////////////////////////////////////////////////
/////////////////////// DataManager //////////////////////////////////////////////

DataManager* DataManager::instance;
DataManager* DataManager::oldInstance;

DataManager* DataManager::getInstance()
{
    if (!instance)
        instance = new DataManager();
    return instance;
}

void DataManager::destroyInstance()
{
    delete instance;
    instance = NULL;
}

DataManager::DataManager()
{
    wdog = make_unique<DataWatchdog>(1000000);
}

DataManager::~DataManager()
{

}

void DataManager::formReplacement()
{
    if (instance) {
        oldInstance = instance;
        instance = new DataManager();
    }
}

void DataManager::commitReplacement(bool ok)
{
    if (ok) {
        delete oldInstance;
    } else {
        delete instance;
        instance = oldInstance;
    }
    oldInstance = NULL;
}

void DataManager::setWatchDogInterval(unsigned int interval)
{
    wdog->setInterval(interval);
}

void DataManager::addNode(DataNode* dn)
{
    dnodes.push_back(std::unique_ptr<DataNode>(dn));
}

void DataManager::addWidget(const std::string& name, QWidget* widget)
{
    widgets[name] = widget;
}

void DataManager::removeWidget(const std::string& name)
{
    widgets.erase(name);
}

std::map<std::string, QWidget*> DataManager::getWidgets()
{
    return widgets;
}

void DataManager::markPluginUsed(const QString& plugin)
{
	usedPlugins[plugin] = true;
}

bool DataManager::isPluginUsed(const QString& plugin)
{
    return usedPlugins.find(plugin) != usedPlugins.end();
}

void DataManager::notifyStop()
{
    MainWindow::getInstance()->notifyStop();
}

void DataManager::start()
{
//    wdog->start();
    for (auto& i : dnodes)
        i->start();
}

void DataManager::stop()
{
//    wdog->stop();
    for (auto& i : dnodes)
        i->stop();
}

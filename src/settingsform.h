 #ifndef SETTINGSFORM_H
#define SETTINGSFORM_H

#include <QString>
#include <QDialog>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QDebug>
#include <QListWidget>

#include <string>
#include <vector>
#include <iosfwd>
#include <memory>

#include <pugixml.hpp>

#include <diagram/view.h>
#include <treedisplay.h>

namespace Ui {
class SettingsForm;
}

class QPushButton;
class PluginPlotInterface;
class IPluginGraph;

/////////////////////////// SettingsForm ////////////////////////////////////////

class MainWindow;

class SettingsForm : public QDialog
{
    Q_OBJECT

public:
    static SettingsForm* getInstance(MainWindow* parent = NULL);
    static void destroyInstance();

    // general settings
    int bufferLength(); QString oldBufferLength;
    bool bufferChanged(bool reset = true);
    unsigned int watchdogInterval();
    void show();

private slots:
    void on_browsePluginButton_clicked();
    void on_addPluginButton_clicked();
    void on_removePluginButton_clicked();
    void on_pluginListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_browseInPlugin_clicked();
    void on_inputSettingsButton_clicked();

	void on_buttonBox_clicked(QAbstractButton *button);

private:
    explicit SettingsForm(MainWindow *parent);
    ~SettingsForm();

    static SettingsForm* instance;
    Ui::SettingsForm *ui;
    MainWindow* parent;

    View* view;
    TreeDisplay* disp;

    void readConfig();
    void updateGraphs();
    bool loadPlugin(QString path);
    bool okNewConfig();
};

#endif // SETTINGSFORM_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <pugixml.hpp>

class QCustomPlot;
class SettingsForm;
class ADSConfig;

class DataSource;
class CircularBuffer;
class Interpreter;
class GraphManager;
class PluginManager;
class QSplitter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class SettingsForm;

public:
    static MainWindow* getInstance();
    static void destroyInstance();

    void notifyStop();

private slots:
    void on_actionStart_triggered();
    void on_actionSettings_triggered();
    void on_actionADS_Configuration_triggered();
    void stopGuiSlot(bool error = false);

signals:
    void stopGuiSig(bool error);

private:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setSplitter(QSplitter* splitter);

    static MainWindow* instance;
    Ui::MainWindow *ui;
    QTimer *timer, *statusTimer;

    DataSource* client;
    CircularBuffer* cbuffer;
    Interpreter* interp;
    std::atomic<bool> stoppable;
};

#endif // MAINWINDOW_H

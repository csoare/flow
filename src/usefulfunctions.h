#ifndef USEFULFUNCTIONS_H
#define USEFULFUNCTIONS_H

#include <vector>
#include <thread>

template <typename T>
void eraseFromVector(std::vector<T>& v, T elem)
{
    for (unsigned int i = 0; i < v.size(); i++)
        if (v[i] == elem) {
            v.erase(v.begin() + i);
            break;
        }
}

class ThreadTask
{
public:
    ThreadTask();
    virtual ~ThreadTask() {}

    void start();
    void stop();

protected:
    virtual void run() = 0;

    std::thread thr;
    bool ongoing;
};

#endif // USEFULFUNCTIONS_H

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = MainWindow::getInstance();
    w->show();

    int ret = a.exec();
    MainWindow::destroyInstance();
    return ret;
}

#include <diagram/diagramscene.h>

#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QInputDialog>
#include <QDebug>

#include <diagram/chip.h>

DiagramScene::DiagramScene(QObject* parent)
    : QGraphicsScene(parent) {}

DiagramScene::DiagramScene(const DiagramScene& dscene)
    : QGraphicsScene(dscene.parent())
{
	for (const auto& i : dscene.chips)
		addChip(new Chip(*i, this));

	for (unsigned int i = 0; i < dscene.chips.size(); i++)
		chips[i]->buildConnections(*dscene.chips[i]);
}

void DiagramScene::setLine(const std::shared_ptr<DataLine>& line)
{
    this->line = line;
    addItem(static_cast<QGraphicsLineItem*>(line.get()));
}

void DiagramScene::setLine(bool persist)
{
    if (!persist)
        removeItem(static_cast<QGraphicsLineItem*>(line.get()));
    line.reset();
}

std::shared_ptr<DataLine> DiagramScene::getLine()
{
    return line;
}

void DiagramScene::addChip(Chip* chip)
{
    chips.push_back(chip);
    addItem(chip);
    chips.back()->setPos(scenePos);
}

Chip* DiagramScene::findChip(const QString& name)
{
    auto found = std::find_if(chips.begin(), chips.end(), [&name](const auto& x){ return x->getName() == name; });
    return found != chips.end() ? *found : nullptr;
}

void DiagramScene::selectedChip(Chip* chip)
{
    selected = chip;
}

void DiagramScene::registerDataNodes()
{
    std::for_each(chips.begin(), chips.end(), [](auto& x){ x->buildDataNode(true); });
    std::for_each(chips.begin(), chips.end(), [](auto& x){ x->buildDataNode(false); });
    std::for_each(chips.begin(), chips.end(), [](auto& x){ DataManager::getInstance()->addNode(x->releaseDataNode()); });
}

std::map<std::string, QWidget*> DiagramScene::getWidgets()
{
    std::map<std::string, QWidget*> ret;
    for (const auto& i : chips) {
        const auto& w = i->getWidgets();
        ret.insert(w.begin(), w.end());
    }
    return ret;
}

void DiagramScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
    scenePos = mouseEvent->scenePos();
    if (line)
        line->setLine(mouseEvent->scenePos());
    QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void DiagramScene::keyPressEvent(QKeyEvent* keyEvent)
{
    if (keyEvent->key() == Qt::Key_Escape && line) {
        line->invalidate();
        setLine(false);
    } else if (keyEvent->key() == Qt::Key_Delete && selected) {
        chips.erase(std::find(chips.begin(), chips.end(), selected));
        removeItem(selected);
        delete selected;
        selected = nullptr;
    } else if (keyEvent->key() == Qt::Key_F2 && selected) {
        bool ok;
        QString txt = QInputDialog::getText(nullptr, "Chip name", "Chip name", QLineEdit::Normal, "", &ok);

        if (ok && !txt.isEmpty())
            selected->setName(txt);
    }

    QGraphicsScene::keyPressEvent(keyEvent);
}

void DiagramScene::save(pugi::xml_node node)
{
    for (const auto& i : chips) {
        auto child = node.append_child("Chip");
        i->save(child);
    }
}

void DiagramScene::load(pugi::xml_node node)
{
    for (pugi::xml_node child = node.child("Chip"); child; child = child.next_sibling("Chip")) {
        addChip(new Chip(QString::fromLocal8Bit(child.attribute("plugName").value()), this));
        chips.back()->load(child);
    }

    unsigned int i = 0;
    for (pugi::xml_node child = node.child("Chip"); child; child = child.next_sibling("Chip"))
        chips[i++]->load(child, true);
}

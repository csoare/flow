/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef CHIP_H
#define CHIP_H

#include <QColor>
#include <QGraphicsPolygonItem>
#include <memory>
#include <QDebug>
#include <pugixml.hpp>

#include <datamanager.h>

class DiagramScene;
class DataLine;

class Chip : public QGraphicsPolygonItem
{
public:
    explicit Chip(const QString& name, DiagramScene* dscene);
    explicit Chip(const Chip& chip, DiagramScene* dscene);
    ~Chip();

    Chip& operator=(const Chip&) = delete;

    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) Q_DECL_OVERRIDE;

    QPointF getLinePoint(DataLine* line);
	bool getType(DataLine* line) const;
	int getPosition(DataLine* line) const;
    void removeLine(DataLine* line);
    DataEntry& getDataEntry(DataLine* line);

	void buildConnections(const Chip& chip);
    void buildDataNode(bool out);
    DataNode* releaseDataNode();

    void setName(const QString& name);
    const QString& getName();
    std::map<std::string, QWidget*> getWidgets();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node, bool doLines = false);


protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event);

private:
    void init();
    void drawIO(QPainter* painter, const std::vector<std::pair<std::string, DataEntry::Domain>>& ios, bool left);
	auto findBy(const DataLine* line) const;
	auto findBy(bool side, int idx) const;
    void setWidgets(bool on);

    std::shared_ptr<IPluginGraph> plugin;
    std::unique_ptr<DataNode> dnode;
    DiagramScene* dscene;

    unsigned int elementsHeight;
    QColor color;
    QString name;
    const QString origName;
    QRectF brect;
    QPointF mousePos;

    std::vector<std::tuple<std::shared_ptr<DataLine>, bool, int>> vlines;
    int selected;
    bool leftSel;
};

class DataLine : public QGraphicsLineItem
{
public:
    DataLine(Chip* start);

    void setOther(Chip* c);
	Chip* getOther(const Chip* c);
    Chip* getEnd(int idx);
    bool hasEnd(Chip* c);

    void setLine();
    void setLine(QPointF point);
    void invalidate();

private:
    Chip* ends[2] = { nullptr, nullptr };
};

#endif // CHIP_H

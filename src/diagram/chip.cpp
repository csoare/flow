/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "chip.h"

#include <QtWidgets>
#include <QInputDialog>
#include <QDebug>

#include <vector>
#include <string>

#include <diagram/diagramscene.h>
#include <pluginmanager.h>
#include <mainwindow.h>


Chip::Chip(const QString& name, DiagramScene* dscene)
	: plugin(PluginManager::getInstance()->getPlugin(name)->initGraphPlugin())
	, name(name)
    , origName(name)
    , dscene(dscene)
    , color(Qt::cyan)
{
    init();
}

Chip::Chip(const Chip& chip, DiagramScene* dscene)
    : plugin(chip.plugin)
    , name(chip.name)
    , origName(chip.origName)
    , dscene(dscene)
    , color(chip.color)
{
	init();
}

void Chip::init()
{
    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);
    elementsHeight = std::max(plugin->getIOProp().first.size(), plugin->getIOProp().second.size());
    if (elementsHeight == 0)
        elementsHeight = 1;

    auto notStop = std::bind(&MainWindow::notifyStop, MainWindow::getInstance());
    plugin->setNotifyStop(notStop);

    setWidgets(true);
}

Chip::~Chip()
{
    for (auto i = vlines.begin(); i != vlines.end(); i++) {
        auto line = std::get<0>(*i).get();
        dscene->removeItem(line);
        line->invalidate();
        i--;
    }

    setWidgets(false);
}

void Chip::setWidgets(bool on)
{
    if (on)
        for (const auto& i : plugin->getWidgets())
            DataManager::getInstance()->addWidget(name.toStdString() + ":" + i.first, i.second);
    else
        for (const auto& i : plugin->getWidgets())
            DataManager::getInstance()->removeWidget(name.toStdString() + ":" + i.first);
}

QRectF Chip::boundingRect() const
{
    return QRectF(0, 0, 110, 14 + elementsHeight * 5 + 10);
}

QPainterPath Chip::shape() const
{
    QPainterPath path;
    path.addRect(5, 14, 100, elementsHeight * 5 + 3);
    return path;
}

void Chip::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);

    QColor fillColor = (option->state & QStyle::State_Selected) ? color.dark(150) : color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.light(125);

    const qreal lod = option->levelOfDetailFromTransform(painter->worldTransform());
    if (lod < 0.2) {
        if (lod < 0.125) {
            painter->fillRect(QRectF(0, 0, 110, 70), fillColor);
            return;
        }

        QBrush b = painter->brush();
        painter->setBrush(fillColor);
        painter->drawRect(13, 13, 97, 57);
        painter->setBrush(b);
        return;
    }

    QPen pen = painter->pen();
    int width = 0;
    if (option->state & QStyle::State_Selected)
        width += 2;

    pen.setWidth(width);
    QBrush b = painter->brush();
    painter->setBrush(QBrush(fillColor.dark(option->state & QStyle::State_Sunken ? 120 : 100)));

    painter->drawRect(QRect(14, 14, 79, elementsHeight * 5 + 3));
    painter->setBrush(b);

    QFont font("Times", 6);
    font.setStyleStrategy(QFont::ForceOutline);
    painter->setFont(font);

    selected = -1;
    painter->drawText(14, 14, 79, elementsHeight * 5 + 3, Qt::AlignCenter, name);

    font.setPointSize(4);
    painter->setFont(font);
    drawIO(painter, plugin->getIOProp().first, true);
    drawIO(painter, plugin->getIOProp().second, false);

    std::for_each(vlines.begin(), vlines.end(), [](const auto& x){ std::get<0>(x)->setLine(); });
}

void Chip::drawIO(QPainter* painter, const std::vector<std::pair<std::string, DataEntry::Domain>>& ios, bool left)
{
    QVector<QLineF> lines;

    for (unsigned int i = 0; i < ios.size(); i++) {
        int ypos = 18 + i * 5;
        int xpos = left ? 5 : 94;
        lines.push_back(QLineF(xpos, ypos, xpos + 8, ypos));

        if (QRectF(xpos, ypos - 2, 8, 4).contains(mousePos)) {
            QPen pen = painter->pen();
            painter->setPen(Qt::red);
            painter->drawLine(lines.back());
            lines.pop_back();
            painter->setPen(pen);

            selected = i;
            leftSel = left;
        }

        painter->drawText(left ? 15 : 80, 15 + i * 5, 10, 5, left ? Qt::AlignLeft : Qt::AlignRight, QString::fromStdString(ios[i].first));
    }

    painter->drawLines(lines);
}

auto Chip::findBy(const DataLine* line) const
{
    return std::find_if(vlines.begin(), vlines.end(), [&line](const auto& x){ return std::get<0>(x).get() == line; });
}

auto Chip::findBy(bool side, int idx) const
{
    return std::find_if(vlines.begin(), vlines.end(),
                        [&side, &idx](const auto& x){ return std::get<1>(x) == side && std::get<2>(x) == idx; });
}

QPointF Chip::getLinePoint(DataLine* line)
{
    auto ret = findBy(line);
    return pos() + QPointF(std::get<1>(*ret) ? 5 : 102, 18 + std::get<2>(*ret) * 5);
}

bool Chip::getType(DataLine* line) const
{
    return std::get<1>(*findBy(line));
}

int Chip::getPosition(DataLine* line) const
{
	return std::get<2>(*findBy(line));
}

void Chip::removeLine(DataLine* line)
{
    auto found  = findBy(line);
    if (found != vlines.end())
        vlines.erase(found);
}

DataEntry& Chip::getDataEntry(DataLine* line)
{
    auto found = findBy(line);
    return dnode->getOut(std::get<1>(*found), std::get<2>(*found));
}

void Chip::buildConnections(const Chip& chip)
{
	setPos(chip.pos());
	for (auto& i : chip.vlines) {
		if (std::get<1>(i))
			continue;
		auto oname = std::get<0>(i)->getOther(&chip)->name;
		auto other = dscene->findChip(oname);
		if (!other)
			continue;

		vlines.push_back(std::make_tuple(std::make_shared<DataLine>(this), false, std::get<2>(i)));
		other->vlines.push_back(std::make_tuple(std::get<0>(vlines.back()), true, chip.getPosition(std::get<0>(i).get())));

		std::get<0>(vlines.back())->setOther(other);
		dscene->addItem(std::get<0>(vlines.back()).get());
	}
}

void Chip::buildDataNode(bool out)
{
    if (out)
        dnode = std::make_unique<DataNode>(plugin);
    else
        for (const auto& i : vlines) {
            if (!std::get<1>(i))
                continue;

            auto line = std::get<0>(i);
            dnode->completeInputs(line->getOther(this)->getDataEntry(line.get()));
        }
}

DataNode* Chip::releaseDataNode()
{
    return dnode.release();
}

void Chip::setName(const QString& name)
{
    setWidgets(false);
    this->name = name;
    setWidgets(true);
}

const QString& Chip::getName()
{
    return name;
}

std::map<std::string, QWidget*> Chip::getWidgets()
{
    auto widgets = plugin->getWidgets();
    std::map<std::string, QWidget*> ret;

    for (auto& i : widgets)
        ret[name.toStdString() + ":" + i.first] = i.second;
    return ret;
}

void Chip::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
    dscene->selectedChip(this);

    if (selected >= 0) {
        std::shared_ptr<DataLine> pline = dscene->getLine();

        if (leftSel && findBy(leftSel, selected) != vlines.end()) {
            QMessageBox::warning(nullptr, "Warning", "Inputs cannot have multiple sources.");
        } else if (!pline) {
            vlines.push_back(std::make_tuple(std::make_shared<DataLine>(this), leftSel, selected));
            dscene->setLine(std::get<0>(vlines.back()));
        } else if (!pline->hasEnd(this)) {
            if (pline->getEnd(0)->getType(pline.get()) == leftSel) {
                QMessageBox::warning(nullptr, "Warning", "Data connection links ends of the same type.");
            } else if (!pline->getOther(this)) {
                vlines.push_back(std::make_tuple(pline, leftSel, selected));
                pline->setOther(this);
                dscene->setLine();
            }
        }
    }
}

void Chip::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
    mousePos = event->pos();
    if ((mousePos.x() >= 5 && mousePos.x() <= 13) || (mousePos.x() >= 94 && mousePos.x() <= 102))
        update();
}

void Chip::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
{
    (void)event;

    if (selected >= 0) {
        auto line = std::get<0>(*findBy(leftSel, selected)).get();
        dscene->removeItem(line);
        line->invalidate();
    } else {
        plugin->show();
        setName(name);
        elementsHeight = std::max(plugin->getIOProp().first.size(), plugin->getIOProp().second.size());
    }
}

void Chip::save(pugi::xml_node node)
{
    node.append_attribute("colour") = color.name().toStdString().c_str();
    node.append_attribute("name") = name.toStdString().c_str();
    node.append_attribute("plugName") = origName.toStdString().c_str();
    node.append_attribute("PosX") = static_cast<int>(pos().x());
    node.append_attribute("PosY") = static_cast<int>(pos().y());
    plugin->save(node.append_child("Plugin"));

    for (const auto& i : vlines) {
        if (std::get<1>(i))
            continue;

        auto line = node.append_child("DataLine");
        auto other = std::get<0>(i)->getOther(this);

        line.append_attribute("other") = other->name.toStdString().c_str();
        line.append_attribute("from") = plugin->getIOProp().second[std::get<2>(i)].first.c_str();
        line.append_attribute("to") = other->plugin->getIOProp().first[std::get<2>(*other->findBy(std::get<0>(i).get()))].first.c_str();
    }
}

void Chip::load(pugi::xml_node node, bool doLines)
{
    if (!doLines) {
        color = QColor(node.attribute("colour").value());
        name = QString::fromLocal8Bit(node.attribute("name").value());
        setPos(QPointF(node.attribute("PosX").as_int(), node.attribute("PosY").as_int()));
        plugin->load(node.child("Plugin"));
        setWidgets(true);
        elementsHeight = std::max(plugin->getIOProp().first.size(), plugin->getIOProp().second.size());
    } else {
        for (pugi::xml_node line = node.child("DataLine"); line; line = line.next_sibling("DataLine")) {
            auto other = dscene->findChip(line.attribute("other").value());
            if (!other)
                continue;

            auto outputs = plugin->getIOProp().second;
            auto from = line.attribute("from").value();
            auto idx = std::find_if(outputs.begin(), outputs.end(), [&from](const auto& x){ return x.first == from; }) - outputs.begin();
            vlines.push_back(std::make_tuple(std::make_shared<DataLine>(this), false, idx));

            auto inputs = other->plugin->getIOProp().first;
            auto to = line.attribute("to").value();
            idx = std::find_if(inputs.begin(), inputs.end(), [&to](const auto& x){ return x.first == to; }) - inputs.begin();
            qDebug() << "other idx" << idx;
            other->vlines.push_back(std::make_tuple(std::get<0>(vlines.back()), true, idx));

            std::get<0>(vlines.back())->setOther(other);
            dscene->addItem(std::get<0>(vlines.back()).get());
        }
    }
}

DataLine::DataLine(Chip* start)
    : ends{start, nullptr} {}

void DataLine::setOther(Chip* c)
{
    ends[1] = c;
}

Chip* DataLine::getOther(const Chip* c)
{
    for (int i = 0; i < 2; i++)
        if (ends[i] == c)
            return ends[1 - i];
    return nullptr;
}

Chip* DataLine::getEnd(int idx)
{
    return ends[idx];
}

bool DataLine::hasEnd(Chip* c)
{
    return std::find(std::begin(ends), std::end(ends), c) != std::end(ends);
}

void DataLine::setLine()
{
    if (ends[1])
        setLine(ends[1]->getLinePoint(this));
}

void DataLine::setLine(QPointF point)
{
    if (ends[0])
        QGraphicsLineItem::setLine(QLineF(ends[0]->getLinePoint(this), point));
}

void DataLine::invalidate()
{
    for (const auto& i : ends)
        if (i)
            i->removeLine(this);
}

#include <treedisplay.h>

#include <QInputDialog>
#include <QMouseEvent>
#include <QMenu>

#include <diagram/diagramscene.h>
#include <datamanager.h>
#include <QDebug>

TreeDisplay::TreeDisplay(DiagramScene* dscene, QWidget* parent)
    : QTreeWidget(parent)
    , dscene(dscene)
{
    setDragEnabled(true);
    setAcceptDrops(true);
    setDragDropMode(QTreeWidget::InternalMove);
    setContextMenuPolicy(Qt::CustomContextMenu);
    setMouseTracking(true);
}

TreeDisplay::TreeDisplay(DiagramScene* dscene, const TreeDisplay& disp)
	: QTreeWidget(static_cast<QWidget*>(disp.parent()))
	, dscene(dscene)
{

}

void TreeDisplay::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::RightButton) {
        auto it = itemAt(event->pos());

        if (it) {
            if (it->parent() && it->text(0).mid(1) != "Splitter")
                return;
            if (!it->parent() && it->childCount())
                return;

            QMenu menu;
            if (it->text(0).mid(1) == "Splitter") {
                menu.addAction("Change orientation");
                menu.addSeparator();
            }
            menu.addAction("VSplitter");
            menu.addAction("HSplitter");
            menu.addSeparator();

            if (it->text(0).mid(1) == "Splitter")
                for (auto& i : DataManager::getInstance()->getWidgets())
                    menu.addAction(QString::fromStdString(i.first));

            auto chosen = menu.exec(event->screenPos().toPoint());
            if (chosen) {
                if (chosen->text() == "Change orientation") {
                    it->setText(0, it->text(0)[0] == 'H' ? "VSplitter" : "HSplitter");
                } else {
                    auto child = new QTreeWidgetItem(it);
                    child->setText(0, chosen->text());
                    allowedContain.insert(child);
                }
            }
        } else {
            bool ok;
            QString txt = QInputDialog::getText(this, "Window name", "Window name", QLineEdit::Normal, "", &ok);

            if (ok && !txt.isEmpty()) {
                auto top = new QTreeWidgetItem(this);
                top->setText(0, txt);
                allowedTop.insert(top);
            }
        }
    }

    QTreeWidget::mousePressEvent(event);
}

void TreeDisplay::keyReleaseEvent(QKeyEvent* event)
{
    auto it = itemAt(mousePos);
    if (event->key() == Qt::Key_Delete && it) {
        if (it->parent()) {
            it->parent()->removeChild(it);
            allowedContain.erase(allowedContain.find(it));
        } else {
            removeItemWidget(it, 0);
            allowedTop.erase(allowedTop.find(it));
        }
    }

    QTreeWidget::keyReleaseEvent(event);
}

void TreeDisplay::mouseMoveEvent(QMouseEvent* event)
{
    mousePos = event->pos();
    QTreeWidget::mouseMoveEvent(event);
}

void TreeDisplay::dropEvent(QDropEvent* event)
{
    auto it = itemAt(event->pos());
    if (it) {
        if (allowedTop.find(it) != allowedTop.end() && allowedContain.find(draggedItem) != allowedContain.end())
            QTreeWidget::dropEvent(event);

        if (allowedContain.find(it) != allowedContain.end())
            QTreeWidget::dropEvent(event);
    } else if (allowedTop.find(draggedItem) != allowedTop.end()) {
        QTreeWidget::dropEvent(event);
    }

    draggedItem = nullptr;
}

void TreeDisplay::dragEnterEvent(QDragEnterEvent* event)
{
    draggedItem = itemAt(event->pos());
    QTreeWidget::dragEnterEvent(event);
}

std::vector<QSplitter*> TreeDisplay::getDisplay()
{
    std::vector<QSplitter*> splitters;
    auto widgets = dscene->getWidgets();

    for (int i = 0; i < topLevelItemCount(); i++)
        splitters.push_back(getDisplay(topLevelItem(i), widgets));
    return splitters;
}

QSplitter* TreeDisplay::getDisplay(QTreeWidgetItem* item, std::map<std::string, QWidget*>& widget)
{
	if (item->columnCount() == 0)
		return nullptr;
    QSplitter* splitter = new QSplitter(item->text(0) == "HSplitter" ? Qt::Horizontal : Qt::Vertical);

    for (int i = 0; i < item->childCount(); i++) {
        auto child = item->child(i);
        auto childName = child->text(0);
        std::map<std::string, QWidget*>::iterator found;

        if (childName.contains("Splitter"))
            splitter->addWidget(getDisplay(child, widget));
        else if ((found = widget.find(childName.toStdString())) != widget.end())
            splitter->addWidget(found->second);
    }

    return splitter;
}

void TreeDisplay::save(pugi::xml_node node)
{
    for (int i = 0; i < topLevelItemCount(); i++) {
        auto top = node.append_child("Window");
        save(top, topLevelItem(i));
    }
}

void TreeDisplay::save(pugi::xml_node node, QTreeWidgetItem* item)
{
    node.append_attribute("name") = item->text(0).toStdString().c_str();
    for (int i = 0; i < item->childCount(); i++) {
        auto child = node.append_child("Widget");
        save(child, item->child(i));
    }
}

void TreeDisplay::load(pugi::xml_node node)
{
    for (pugi::xml_node top = node.child("Window"); top; top = top.next_sibling("Window"))
        allowedTop.insert(load(top, this));
}

QTreeWidgetItem* TreeDisplay::load(pugi::xml_node node, auto item)
{
    auto ichild = new QTreeWidgetItem(item);
    ichild->setText(0, node.attribute("name").value());

    for (pugi::xml_node nchild = node.child("Widget"); nchild; nchild = nchild.next_sibling("Widget"))
        allowedContain.insert(load(nchild, ichild));
    return ichild;
}

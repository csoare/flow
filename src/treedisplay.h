#ifndef TREEDISPLAY_H
#define TREEDISPLAY_H

#include <QTreeWidget>
#include <QPoint>
#include <QSplitter>
#include <set>
#include <pugixml.hpp>

class DiagramScene;

class TreeDisplay : public QTreeWidget
{
public:
    explicit TreeDisplay(DiagramScene* dscene, QWidget* parent = nullptr);
    explicit TreeDisplay(DiagramScene* dscene, const TreeDisplay& disp);

    std::vector<QSplitter*> getDisplay();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node);

private:
    QSplitter* getDisplay(QTreeWidgetItem* item, std::map<std::string, QWidget*>& widgets);

    void keyReleaseEvent(QKeyEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);

    void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent* event);

    void save(pugi::xml_node node, QTreeWidgetItem* item);
    QTreeWidgetItem* load(pugi::xml_node node, auto item);

    DiagramScene* dscene;
    QPoint mousePos;
    std::set<QTreeWidgetItem*> allowedContain, allowedTop;
    QTreeWidgetItem* draggedItem = nullptr;
};

#endif // TREEDISPLAY_H

#ifndef GENERALBUFFER_H
#define GENERALBUFFER_H

#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <memory>
#include <vector>

template <typename T>
class GeneralBuffer
{
public:
    GeneralBuffer(int cap);
    GeneralBuffer(const GeneralBuffer& buf);
    ~GeneralBuffer();

    GeneralBuffer& operator=(const GeneralBuffer&) = delete;

    void reset();
    void setWriterEnabled(bool ok);

    int beginRead(bool merge = true, bool blocking = true);
    const T& operator[](int x);
    const T& at(int x);
    void endRead(int count);
    void endRead();

    int beginWrite();
    bool add(T val);
    int getSize();
    int getLimit();
    void endWrite();

protected:
    struct Writer {
        Writer(int capacity);
        ~Writer();

        T* buffer;
        int right, ongoingRight, writeLimit, capacity;
        bool writerEnabled;

        std::shared_timed_mutex mutex;
        std::condition_variable_any hasSpace, hasElements;
        std::vector<GeneralBuffer*> memebers;
    };

    std::shared_ptr<Writer> writer;
    int left, size;

private:
    int maxCount();
    int getCount(int left, bool merge = true);

    bool beganRead = false;
};

template <typename T>
GeneralBuffer<T>::Writer::Writer(int capacity)
    : buffer(new T[capacity])
    , right(0), ongoingRight(0), writeLimit(0), capacity(capacity)
    , writerEnabled(false) {}

template <typename T>
GeneralBuffer<T>::Writer::~Writer()
{
    delete[] buffer;
}

template <typename T>
GeneralBuffer<T>::GeneralBuffer(int cap)
    : writer(std::make_shared<Writer>(cap))
    , left(0), size(0)
{

}

template <typename T>
GeneralBuffer<T>::GeneralBuffer(const GeneralBuffer& buf)
    : writer(buf.writer)
    , left(0), size(0)
{
    writer->memebers.push_back(this);
}

template <typename T>
GeneralBuffer<T>::~GeneralBuffer()
{
    for (unsigned int i = 0; i < writer->memebers.size(); i++)
        if (writer->memebers[i] == this) {
            writer->memebers.erase(writer->memebers.begin() + i);
            break;
        }
}

template <typename T>
void GeneralBuffer<T>::reset()
{
    std::lock_guard<std::shared_timed_mutex> lock(writer->mutex);
    writer->right = writer->ongoingRight = 0;
    writer->writeLimit = 0;
    writer->writerEnabled = false;

    for (unsigned int i = 0; i < writer->memebers.size(); i++)
        writer->memebers[i]->left = 0;
}

template <typename T>
void GeneralBuffer<T>::setWriterEnabled(bool ok)
{
    std::unique_lock<std::shared_timed_mutex> lock(writer->mutex);
    writer->writerEnabled = ok;
    writer->hasElements.notify_all();
}

template <typename T>
int GeneralBuffer<T>::getCount(int left, bool merge)
{
    if (left <= writer->right)
        return writer->right - left;
    return writer->capacity - left + (merge ? writer->right : 0);
}

template <typename T>
int GeneralBuffer<T>::beginWrite()
{
    std::unique_lock<std::shared_timed_mutex> lock(writer->mutex);

    int count;
    while ((count = maxCount()) == writer->capacity - 1)
        writer->hasSpace.wait(lock);

    writer->ongoingRight = writer->right;
    writer->writeLimit = writer->capacity - count - 1;

    return writer->writeLimit;
}

template <typename T>
bool GeneralBuffer<T>::add(T val)
{
    if (!writer->writeLimit)
        return false;

    writer->buffer[writer->ongoingRight] = val;
    writer->ongoingRight = (writer->ongoingRight + 1) % writer->capacity;
    writer->writeLimit--;

    if (!writer->writeLimit)
        endWrite();
    return true;
}

template <typename T>
void GeneralBuffer<T>::endWrite()
{
    std::unique_lock<std::shared_timed_mutex> lock(writer->mutex);
    if (writer->right != writer->ongoingRight) {
        writer->right = writer->ongoingRight;
        writer->hasElements.notify_all();
    }
    writer->writeLimit = 0;
}

template <typename T>
int GeneralBuffer<T>::beginRead(bool merge, bool blocking)
{
    beganRead = true;
    std::shared_lock<std::shared_timed_mutex> lock(writer->mutex);

    while ((size = getCount(left, merge)) == 0 && blocking && writer->writerEnabled)
        writer->hasElements.wait(lock);
    return size;
}

template <typename T>
const T& GeneralBuffer<T>::operator[](int x)
{
    x = x >= 0 ? x : 0;
    return writer->buffer[(left + x) % writer->capacity];
}

template <typename T>
const T& GeneralBuffer<T>::at(int x)
{
    return operator[](x);
}

template <typename T>
void GeneralBuffer<T>::endRead(int count)
{
    if (!beganRead)
        return;

    std::shared_lock<std::shared_timed_mutex> lock(writer->mutex);
    beganRead = false;
    left = (left + count) % writer->capacity;
    writer->hasSpace.notify_one();
}

template <typename T>
void GeneralBuffer<T>::endRead()
{
    endRead(size);
}

template <typename T>
int GeneralBuffer<T>::getLimit()
{
    return writer->writeLimit;
}

template <typename T>
int GeneralBuffer<T>::getSize()
{
    return size;
}

template <typename T>
int GeneralBuffer<T>::maxCount()
{
    int maximum = 0;
    for (unsigned int i = 0; i < writer->memebers.size(); i++) {
        int count = getCount(writer->memebers[i]->left);

        if (maximum < count)
            maximum = count;
    }
    return maximum;
}

#endif // GENERALBUFFER_H


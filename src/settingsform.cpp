#include "settingsform.h"
#include "ui_settingsform.h"

#include "mainwindow.h"
#include "pluginmanager.h"
#include "usefulfunctions.h"
#include "datamanager.h"

#include <diagram/view.h>
#include <diagram/diagramscene.h>

#include <QFileDialog>
#include <QComboBox>
#include <QCheckBox>
#include <qcustomplot.h>

#include <fstream>
#include <string>
#include <algorithm>

#include <unistd.h>
#include <pwd.h>
#include <dlfcn.h>
#include <stdexcept>

using namespace std;

/////////////////////////// DataModifier ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

static QPalette getPalette(bool ok)
{
    QPalette palette;
    if (!ok) {
        palette.setColor(QPalette::Background, Qt::red);
        palette.setColor(QPalette::Base, Qt::red);
    }
    return palette;
}

/////////////////////////// SettingsForm ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

SettingsForm* SettingsForm::instance;

SettingsForm::SettingsForm(MainWindow* parent) :
    QDialog(parent),
    ui(new Ui::SettingsForm),
    parent(parent)
{
    ui->setupUi(this);

    // general tab
    ui->bufLenLineEdit->setText(QString::number(100000));

    ui->tabDiagram->layout()->addWidget(view = new View("proba"));
    ui->tabDisplay->layout()->addWidget(disp = new TreeDisplay(static_cast<DiagramScene*>((view->view()->scene()))));

    readConfig();
    okNewConfig();
}

SettingsForm::~SettingsForm()
{
    delete ui;
}

SettingsForm* SettingsForm::getInstance(MainWindow* parent)
{
    if (!instance)
        instance = new SettingsForm(parent ? parent : MainWindow::getInstance());
    return instance;
}

void SettingsForm::destroyInstance()
{
    delete instance;
    instance = NULL;
}

static string getConfigDir()
{
    passwd* pass = getpwuid(getuid());
    string file(pass->pw_name);
    file = "/home/" + file + "/.bmirc";
    return file;
}

void SettingsForm::readConfig()
{
    pugi::xml_document doc;
    if (doc.load_file(getConfigDir().c_str()).status != pugi::status_ok)
        return;

    pugi::xml_node root = doc.child("bmi");
    ui->bufLenLineEdit->setText(QString::fromLocal8Bit(root.attribute("bufferLength").value()));
    ui->wdogSpin->setValue(root.attribute("watchdog").as_int());

    for (pugi::xml_node node = root.child("Plugin"); node; node = node.next_sibling("Plugin")) {
        loadPlugin(node.attribute("path").value());
        PluginManager::getInstance()->getPlugin(node.attribute("name").value())->load(node);
    }

	static_cast<DiagramScene*>(view->view()->scene())->load(root.child("Diagram"));
    disp->load(root.child("TreeDisplay"));
}

void SettingsForm::updateGraphs()
{
    view->registerDataNodes();

    DataManager::commitReplacement(true);
    DataManager::getInstance()->setWatchDogInterval(ui->wdogSpin->value() * 1000);
	if (disp->getDisplay().size()) {
		qDebug() << "instrat";
		MainWindow::getInstance()->setSplitter(disp->getDisplay().front());
	}
}


bool SettingsForm::okNewConfig()
{
    try {
        updateGraphs();
    } catch (const runtime_error& e) {
        QMessageBox::warning(this, "Error", QString::fromUtf8(e.what()));
        return false;
    }

    if (ui->bufLenLineEdit->text().toInt() == 0) {
        QMessageBox::warning(this, "Error", "Invalid buffer length");
        return false;
    }

    oldBufferLength = ui->bufLenLineEdit->text();
    return true;
}

int SettingsForm::bufferLength() { return ui->bufLenLineEdit->text().toInt(); }
unsigned int SettingsForm::watchdogInterval() { return ui->wdogSpin->value(); }

bool SettingsForm::bufferChanged(bool reset)
{
    bool ok = ui->bufLenLineEdit->isModified();
    if (reset) {
        ui->bufLenLineEdit->setModified(false);
    }
    return ok;
}

///////////////// Plugin Manager ////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void SettingsForm::on_browsePluginButton_clicked()
{
    ui->pluginPathLineEdit->setText(
                QFileDialog::getOpenFileName(
                    this, "Plugin", ui->pluginPathLineEdit->text()));
}

bool SettingsForm::loadPlugin(QString path)
{
    try {
        QString pname = PluginManager::getInstance()->addPlugin(path, this);
        ui->pluginListWidget->addItem(pname);
        return true;
    } catch (const exception& ex) {
        QMessageBox::warning(this, "Error", QString::fromLocal8Bit(ex.what()) + path);
        return false;
    }
}

void SettingsForm::on_addPluginButton_clicked()
{
    loadPlugin(ui->pluginPathLineEdit->text());
}

void SettingsForm::on_removePluginButton_clicked()
{
    int x = ui->pluginListWidget->currentRow();
	if (x < 0)
		return;

	if (DataManager::getInstance()->isPluginUsed(ui->pluginListWidget->currentItem()->text())) {
        QMessageBox::warning(this, "Error", "The plugin is being used !");
        return;
    }

    PluginManager::getInstance()->removePlugin(x);
    ui->pluginListWidget->takeItem(x);
}

void SettingsForm::on_pluginListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    PluginManager::getInstance()->getPlugin(item->text())->show();
}

void SettingsForm::on_browseInPlugin_clicked()
{
    ui->inpluginLineEdit->setText(
                QFileDialog::getOpenFileName(
                    this, "Input plugin", ui->inpluginLineEdit->text()));
    InputPluginInterface::getInstance(ui->inpluginLineEdit->text().toStdString());
}

void SettingsForm::on_inputSettingsButton_clicked()
{
    if (InputPluginInterface::getInstance())
        InputPluginInterface::getInstance()->showSettings();
}

void SettingsForm::on_buttonBox_clicked(QAbstractButton *button)
{
	QDialogButtonBox::StandardButton bflag = ui->buttonBox->standardButton(button);
	switch (bflag) {
	case QDialogButtonBox::Save: {
		if (!okNewConfig())
			return;

		pugi::xml_document doc;
		pugi::xml_node root = doc.append_child("bmi");
		root.append_attribute("bufferLength") = static_cast<const char*>(ui->bufLenLineEdit->text().toLocal8Bit());
		root.append_attribute("watchdog") = ui->wdogSpin->value();

		const vector<QString>& paths = PluginManager::getInstance()->getPluginPaths();
		const vector<IPlugin*>& plugins = PluginManager::getInstance()->getPlugins();
		for (unsigned int i = 0; i < paths.size(); i++) {
			pugi::xml_node node = root.append_child("Plugin");
			node.append_attribute("name") = static_cast<const char*>(plugins[i]->getName().toLocal8Bit());
			node.append_attribute("path") = static_cast<const char*>(paths[i].toLocal8Bit());
            plugins[i]->save(node);
		}

        auto dchild = root.append_child("Diagram");
        static_cast<DiagramScene*>(view->view()->scene())->save(dchild);

        auto tchild = root.append_child("TreeDisplay");
        disp->save(tchild);
		doc.save_file(getConfigDir().c_str());
	}
		break;

	case QDialogButtonBox::Ok:
	case QDialogButtonBox::Apply:

		if (okNewConfig() && bflag == QDialogButtonBox::Ok) {
			close();
		}
		break;

	case QDialogButtonBox::Cancel:
		ui->bufLenLineEdit->setText(oldBufferLength);
        DataManager::getInstance()->commitReplacement(false);
		close();
		break;

	default:
		break;
	}
}

void SettingsForm::show()
{
	DataManager::getInstance()->formReplacement();
    QDialog::show();
}

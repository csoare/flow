#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qcustomplot.h>
#include "settingsform.h"
#include "pluginmanager.h"
#include "datamanager.h"

#include <QSplitter>
#include <QVBoxLayout>

#include <iostream>

using namespace std;

MainWindow* MainWindow::instance;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    cbuffer = NULL; client = NULL; interp = NULL;
    instance = this;

    timer = new QTimer();
    timer->setInterval(33);

    statusTimer = new QTimer();
    statusTimer->setInterval(300);

    SettingsForm::getInstance(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(run()));
    connect(statusTimer, SIGNAL(timeout()), this, SLOT(statusFunc()));
    connect(this, SIGNAL(stopGuiSig(bool)), this, SLOT(stopGuiSlot(bool)));
}

MainWindow::~MainWindow()
{
    if (timer->isActive()) {
        InputPluginInterface::getInstance()->stop();
        stopGuiSlot();
    }

    delete timer;
    SettingsForm::destroyInstance();
	InputPluginInterface::destroyInstance();
    DataManager::destroyInstance();
    PluginManager::destroyInstance();
    delete ui;
}

MainWindow* MainWindow::getInstance()
{
    if (!instance)
        new MainWindow();
    return instance;
}

void MainWindow::destroyInstance()
{
    delete instance;
    instance = NULL;
}

void MainWindow::notifyStop()
{
    if (!stoppable)
        return;

    stoppable = false;
    std::thread([this](){ emit this->stopGuiSig(true); }).detach();
}

void MainWindow::stopGuiSlot(bool error)
{
    DataManager::getInstance()->stop();
    timer->stop();
    statusTimer->stop();

    statusBar()->showMessage("Idle");
    ui->actionStart->setIcon(QIcon(":/prefix/icons/Play-icon.png"));
    ui->actionStart->setText("Start");

    if (error) {
        QString errmsg = "Communication with the device has been lost !";
        QMessageBox::critical(this, "Error", errmsg);
    }
}

void MainWindow::on_actionStart_triggered()
{
    if (!timer->isActive()) {
        timer->start();
        statusTimer->start();

        ui->actionStart->setIcon(QIcon(":/prefix/icons/Stop-icon.png"));
        ui->actionStart->setText("Stop");
        stoppable = true;
        DataManager::getInstance()->start();
    } else {
        stopGuiSlot();
    }
}

void MainWindow::on_actionSettings_triggered()
{
    if (!timer->isActive())
        SettingsForm::getInstance()->show();
}

void MainWindow::setSplitter(QSplitter* splitter)
{
    QLayoutItem* widget;
    while ((widget = ui->verticalLayout->takeAt(0)) != nullptr)
        ui->verticalLayout->removeWidget(widget->widget());
    ui->verticalLayout->addWidget(splitter);
}

void MainWindow::on_actionADS_Configuration_triggered()
{
    if (InputPluginInterface::getInstance())
        InputPluginInterface::getInstance()->showConfig();
    else
        QMessageBox::warning(this, "Error", "No input device plugin added !");
}

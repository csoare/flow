#-------------------------------------------------
#
# Project created by QtCreator 2014-07-15T17:12:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport opengl

TARGET = BMInterface
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++14
CONFIG += debug

CONFIG(debug){
    DESTDIR = build
    OBJECTS_DIR = build/obj
    MOC_DIR = build/moc
    RCC_DIR = build/rcc
    UI_DIR = build/ui
}

LIBS += -ldl -lpugixml -lqcustomplot

INCLUDEPATH += src

SOURCES += src/*.cpp \
    src/diagram/*.cpp

HEADERS  += src/*.h \
    src/diagram/chip.h \
    src/diagram/diagramscene.h \
    src/diagram/view.h

FORMS    += ui/*

#DEPENDPATH += communication/

RESOURCES += res/iconsRes.qrc
